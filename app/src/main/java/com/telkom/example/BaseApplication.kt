package com.telkom.example

import android.app.Application
import com.telkom.example.di.AppComponent
import com.telkom.example.di.DaggerAppComponent
import com.telkom.example.di.Provider

class BaseApplication : Application() {
    private val appComponent: AppComponent by lazy {
        initializeComponent()
    }

    private fun initializeComponent(): AppComponent {
        return DaggerAppComponent.factory().build(this)
    }

    override fun onCreate() {
        super.onCreate()
        Provider.appComponent = appComponent
    }
}