package com.telkom.example.data.repository

import com.telkom.example.data.entities.BaseSourceApi
import com.telkom.example.data.entities.UserSourceApi
import io.reactivex.Single

interface UserRepositoryContract {

    fun getUsers(page: Int): Single<BaseSourceApi<List<UserSourceApi>>>

}