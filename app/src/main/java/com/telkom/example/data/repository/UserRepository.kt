package com.telkom.example.data.repository

import com.telkom.example.data.entities.BaseSourceApi
import com.telkom.example.data.entities.UserSourceApi
import com.telkom.example.data.network.ApiService
import io.reactivex.Single

class UserRepository(private val apiService: ApiService) : UserRepositoryContract {
    override fun getUsers(page: Int): Single<BaseSourceApi<List<UserSourceApi>>> =
        apiService.getUsers(page)
}