package com.telkom.example.data.entities

import com.google.gson.annotations.SerializedName

data class BaseSourceApi<T>(
    @SerializedName("data")
    val `data`: T?,
    @SerializedName("error")
    val error: String?,
    @SerializedName("page")
    val page: Int?,
    @SerializedName("per_page")
    val perPage: Int?,
    @SerializedName("total")
    val total: Int?,
    @SerializedName("total_pages")
    val totalPages: Int?
)