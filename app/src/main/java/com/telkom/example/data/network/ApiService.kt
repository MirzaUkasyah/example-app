package com.telkom.example.data.network

import com.telkom.example.data.entities.BaseSourceApi
import com.telkom.example.data.entities.UserSourceApi
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("users")
    fun getUsers(@Query("page") page: Int): Single<BaseSourceApi<List<UserSourceApi>>>

}