package com.telkom.example.utils

interface ViewModelOwner<T : BaseViewModel> {
    val viewModel: T
}
