package com.telkom.example.utils

interface DiffCallbackListener<T> {
    fun areItemsTheSame(oldItem: T, newItem: T): Boolean
}