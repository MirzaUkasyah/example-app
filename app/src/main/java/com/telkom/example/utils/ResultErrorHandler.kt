package com.telkom.example.utils

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.telkom.example.data.entities.BaseSourceApi
import retrofit2.HttpException

fun getResultStateError(error: Throwable): ResultState {
    return when (error) {
        is HttpException -> {
            val errorResponse = error.response()?.errorBody()?.string() ?: ""
            when (error.code()) {
                400 -> {
                    ResultState.BadRequest(
                        throwable = Throwable(
                            getMessageFromBody(
                                throwable = error,
                                errResponse = errorResponse
                            )
                        )
                    )
                }
                401 -> {
                    ResultState.Unauthorized(
                        throwable = Throwable(
                            getMessageFromBody(
                                throwable = error,
                                errResponse = errorResponse
                            )
                        )
                    )
                }
                404 -> {
                    ResultState.NotFound(
                        throwable = Throwable(
                            getMessageFromBody(
                                throwable = error,
                                errResponse = errorResponse
                            )
                        )
                    )
                }
                403 -> {
                        ResultState.Forbidden(
                            throwable = Throwable(
                                getMessageFromBody(
                                    throwable = error,
                                    errResponse = errorResponse
                                )
                            )
                        )
                }
                409, 406 -> {
                    ResultState.Conflict(
                        throwable = Throwable(
                            getMessageFromBody(
                                throwable = error,
                                errResponse = errorResponse
                            )
                        )
                    )

                }
                502, 503 -> {
                    ResultState.BadGateway(
                        throwable = Throwable(
                            getMessageFromBody(
                                throwable = error,
                                errResponse = errorResponse
                            )
                        )
                    )

                }
                else -> {
                    ResultState.Error(throwable = error)
                }
            }
        }
        else -> {
            ResultState.Error(throwable = error)
        }
    }

}

private fun getMessageFromBody(throwable: HttpException, errResponse: String): String {
    val type = object : TypeToken<BaseSourceApi<String>>() {}.type
    var errorResponse: BaseSourceApi<String>? = null
    return try {
        throwable.response()?.errorBody()?.let {
            errorResponse = Gson().fromJson(errResponse, type)
        }
        errorResponse?.error ?: "Terjadi gangguan pada server [${throwable.code()}] "

    } catch (error: Exception) {

        errorResponse?.error ?: "Terjadi gangguan pada server [${throwable.code()}] "
    }
}

fun showToastErrorMessage(state: ResultState, context: Context) {
    when (state) {
        is ResultState.Conflict -> {
            state.throwable.message?.let { context.toastLong(it) }
        }
        is ResultState.BadRequest -> {
            state.throwable.message?.let { context.toastLong(it) }
        }
        is ResultState.NoConnection -> {
            state.throwable.message?.let { context.toastLong(it) }
        }
        is ResultState.NotFound -> {
            state.throwable.message?.let { context.toastLong(it) }
        }
        is ResultState.Forbidden -> {
            state.throwable.message?.let { context.toastLong(it) }
        }
        is ResultState.Unauthorized -> {
            state.throwable.message?.let { context.toastLong(it) }
        }
        is ResultState.Error -> {
            state.throwable.message?.let { context.toastLong(it) }
        }
    }

}