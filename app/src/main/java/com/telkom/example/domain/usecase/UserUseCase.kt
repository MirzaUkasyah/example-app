package com.telkom.example.domain.usecase

import com.telkom.example.data.repository.UserRepositoryContract
import com.telkom.example.domain.entities.UserEntity
import com.telkom.example.domain.mapper.map
import com.telkom.example.utils.ResultState
import io.reactivex.Single

class UserUseCase(private val userRepositoryContract: UserRepositoryContract) : UserUseCaseContract {
    override fun getUsers(page: Int): Single<ResultState> {
        return userRepositoryContract.getUsers(page).map {
            ResultState.Success(it.map(),"")
        }
    }
}