package com.telkom.example.domain.entities

data class UserEntity(
    val id: Int,
    val email: String,
    val firstName: String,
    val lastName: String,
    val image: String
)