package com.telkom.example.domain.mapper

import com.telkom.example.data.entities.BaseSourceApi
import com.telkom.example.data.entities.UserSourceApi
import com.telkom.example.domain.entities.UserEntity


fun BaseSourceApi<List<UserSourceApi>>.map() = data?.map()

fun List<UserSourceApi>.map(): List<UserEntity> = this.map {
    it.map()
}

fun UserSourceApi?.map(): UserEntity {
    return UserEntity(
        id = this?.id ?: 0,
        email = this?.email ?: "-",
        firstName = this?.firstName ?: "-",
        lastName = this?.lastName ?: "-",
        image = this?.avatar ?: "-",
    )
}