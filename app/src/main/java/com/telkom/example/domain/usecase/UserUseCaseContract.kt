package com.telkom.example.domain.usecase

import com.telkom.example.utils.ResultState
import io.reactivex.Single

interface UserUseCaseContract {
    fun getUsers(page: Int): Single<ResultState>
}