package com.telkom.example.presentation.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.telkom.example.databinding.ItemUserBinding
import com.telkom.example.domain.entities.UserEntity
import com.telkom.example.utils.BaseAdapter
import com.telkom.example.utils.DiffCallbackListener

class UserAdapter : BaseAdapter<UserEntity, ItemUserBinding>(diffCallbackListener) {

    override fun createViewHolder(inflater: LayoutInflater, container: ViewGroup) =
        ItemUserBinding.inflate(inflater, container, false)

    override fun bind(binding: ItemUserBinding, item: UserEntity, position: Int, count: Int) {
        binding.apply {
            tvName.text = item.firstName
            tvEmail.text = item.email
            Glide.with(root.context)
                .load(item.image)
                .apply(RequestOptions.bitmapTransform(RoundedCorners(10)))
                .into(ivAvatar)
        }
    }

    companion object {
        val diffCallbackListener = object : DiffCallbackListener<UserEntity> {
            override fun areItemsTheSame(
                oldItem: UserEntity,
                newItem: UserEntity
            ) =
                oldItem == newItem
        }
    }
}