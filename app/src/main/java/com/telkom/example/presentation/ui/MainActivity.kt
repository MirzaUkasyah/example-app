package com.telkom.example.presentation.ui

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.telkom.example.databinding.ActivityMainBinding
import com.telkom.example.di.DaggerAppComponent
import com.telkom.example.domain.entities.UserEntity
import com.telkom.example.presentation.viewModel.MainViewModel
import com.telkom.example.utils.*
import javax.inject.Inject

class MainActivity : BaseDaggerActivity(), ViewModelOwner<MainViewModel> {
    @Inject
    override lateinit var viewModel: MainViewModel
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private lateinit var adapter: UserAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        observe(viewModel.resultUsers, ::manageResultUser)
        initView()
        viewModel.getUsers()
    }

    private fun initView(){
        binding.apply {
            adapter = UserAdapter()
            rvUsers.adapter = adapter
        }
    }

    private fun manageResultUser(resultState: ResultState) {
        when (resultState) {
            is ResultState.Success<*> -> {
                val data = resultState.data as List<UserEntity>
                adapter.setItems(data)
            }
            is ResultState.Loading -> Unit
            is ResultState.HideLoading -> Unit
            else -> showToastErrorMessage(resultState, this)
        }
    }

    override fun injectComponent() {
        DaggerAppComponent.factory().build(application).inject(this)
    }
}