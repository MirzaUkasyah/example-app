package com.telkom.example.presentation.viewModel

import androidx.lifecycle.MutableLiveData
import com.telkom.example.domain.usecase.UserUseCaseContract
import com.telkom.example.utils.BaseViewModel
import com.telkom.example.utils.ResultState
import com.telkom.example.utils.getResultStateError
import com.telkom.example.utils.singleIo
import javax.inject.Inject

class MainViewModel@Inject constructor(
    private val userUseCaseContract: UserUseCaseContract
) : BaseViewModel() {
    
    val resultUsers = MutableLiveData<ResultState>()
    
    fun getUsers(){
        val disposable = userUseCaseContract.getUsers(page = 1)
            .compose(singleIo())
            .doOnSubscribe {
                resultUsers.value = ResultState.Loading
            }
            .subscribe({ resultState ->
                resultUsers.value = ResultState.HideLoading
                resultUsers.value = resultState

            }, {
                resultUsers.value = ResultState.HideLoading
                resultUsers.value = getResultStateError(it)

            })
        addDisposable(disposable)
    }
}