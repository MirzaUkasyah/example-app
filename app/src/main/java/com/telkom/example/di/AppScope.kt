package com.telkom.example.di

import javax.inject.Scope

@Scope
annotation class AppScope()
