package com.telkom.example.di

import com.telkom.example.data.network.ApiService
import com.telkom.example.utils.NetworkHelper
import dagger.Module
import dagger.Provides

@Module
class NetworkModule {
    @Provides
    @NetworkScope
    fun provideService() : ApiService {
        return NetworkHelper.retrofitClient().create(ApiService::class.java)
    }
}