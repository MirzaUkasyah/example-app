package com.telkom.example.di

import javax.inject.Qualifier

@Qualifier
annotation class NetworkScope()
