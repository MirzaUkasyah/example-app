package com.telkom.example.di.user

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.telkom.example.di.AppScope
import com.telkom.example.presentation.viewModel.MainViewModel
import com.telkom.example.utils.ViewModelFactory
import com.telkom.example.utils.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class UserViewModelModule {
    @AppScope
    @Binds
    internal abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel
}