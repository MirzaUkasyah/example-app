package com.telkom.example.di

import com.telkom.example.data.network.ApiService
import com.telkom.example.data.repository.UserRepository
import com.telkom.example.data.repository.UserRepositoryContract
import com.telkom.example.domain.usecase.UserUseCase
import com.telkom.example.domain.usecase.UserUseCaseContract
import dagger.Module
import dagger.Provides

@Module(includes = [NetworkModule::class])
class AppModule {
    @AppScope
    @Provides
    internal fun provideUserUseCase(repositoryContract: UserRepositoryContract): UserUseCaseContract {
        return UserUseCase(repositoryContract)
    }

    @AppScope
    @Provides
    internal fun provideUserRepository(@NetworkScope apiServices: ApiService): UserRepositoryContract {
        return UserRepository(apiServices)
    }
}