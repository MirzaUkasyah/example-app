package com.telkom.example.di

import android.app.Application
import com.telkom.example.di.user.UserViewModelModule
import com.telkom.example.presentation.ui.MainActivity
import dagger.BindsInstance
import dagger.Component

@AppScope
@Component(
    modules = [
        AppModule::class,
    ]
)
interface AppComponent {

    @Component.Factory
    interface Builder {

        fun build(@BindsInstance application: Application): AppComponent

    }

    fun inject(activity: MainActivity)
}